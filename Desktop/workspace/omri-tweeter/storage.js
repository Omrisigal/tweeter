import net from 'net'

import fs from 'fs';
let mySqlFile;
let client = new net.Socket();

client.connect(1337, '127.0.0.1', function() {
	console.log('Connected');
	client.write(`${'Hello, server! Love, Client.'}`);
});

client.on('data', function(data) {
    try{
        data = JSON.parse(data.toString());
        if(data.hasOwnProperty('file_name')){
            mySqlFile = fs.createWriteStream(data.file_name, { flags: 'a' });
            mySqlFile.write(data.toString());
        }
        else{
            console.log(data.message);
        }

    }
    catch(err){
            mySqlFile.write(data.toString());
        
        
    }
});
client.on('end',()=>{
    mySqlFile.close();;

})
client.on('close', function() {
	console.log('Connection closed');
});