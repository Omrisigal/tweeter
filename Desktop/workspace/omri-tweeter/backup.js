
import fs from 'fs';
import {exec, spawn} from 'child_process';
import cron from 'node-cron';
import net from 'net'
import log from '@ajar/marker';


try{
    const server = net.createServer();
    server.on('connection', (socket)=> {
        socket.write(JSON.stringify({message:'Echo server\r\n'}));
        cron.schedule(' * * * * * * ', async () => {
            let timestamp = Date.now();
            let date = new Date(timestamp);
            date = (date.toISOString().replace(/[.,\/#!$%\^&\*;:{}=\-_`~()]/g,"-"));
            let obj = {file_name:date+'.dump.sql'};
            socket.write(JSON.stringify(obj));
            let child = exec('docker exec mysql-db mysqldump -u root --password=qwerty world')
            for await(const data of child.stdout){
                socket.write(data);
            }
            console.log('Backing up DB '+ date);
            socket.write(JSON.stringify({message:'Backing up DB '+ date}));

            socket.end;
          });
          socket.on("end", () => {
            server.close(() => { console.log("\nTransfer is done!") });
        })
    })

    server.listen(1337, '127.0.0.1',() => log.v('✨ ⚡Server is up  🚀'));
   
}
catch(err){
    console.log(err.message);
}

